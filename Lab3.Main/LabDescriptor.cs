﻿using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(void);
        public static Type I2 = typeof(void);
        public static Type I3 = typeof(void);

        public static Type Component = typeof(void);

        public static GetInstance GetInstanceOfI1 = (component) => null;
        public static GetInstance GetInstanceOfI2 = (component) => null;
        public static GetInstance GetInstanceOfI3 = (component) => null;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(void);
        public static Type MixinFor = typeof(void);

        #endregion
    }
}
